********************************************
		Hangman v2
********************************************

The classic game of hangman recreated in the C# console.

********************************************

The game code is quite self explanatory, there are some comments but most of the code is understandable from the function and variable names.

If you use any of this code, please acknowledge:
	Antony Lloyd
	only-antony.com